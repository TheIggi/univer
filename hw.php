<?php 

class YClass{
    public function y($x) {
        return $x*$x;
    }
	
    public function Main(){
        MainClass::test($this);
    }
}

class MainClass{
    public function test($Y) {
        echo $Y->y(5);
    }
}


$a = new YClass;
$a->Main();


function solve($a, $b, $c){
    if($a == 0){ 
        if($b == 0) return "No solution";
        return - $c / $b;
    }
	
    $disc = pow($b, 2) - 4*$a*$c;
    if($disc < 0) return "No solution";
    if($disc == 0) return - $b / 2*$a;
	 
    $disc = sqrt($disc);
    return (- $b + $disc) / 2*$a . ' : ' . (- $b - $disc) / 2*$a;
	 
}
 
function square($a){
    return sqrt(3) / 4 * $a;
}

echo '<br>';
echo square(4);
echo '<br>';
echo solve(1, 10, -2);

?>







































